import xml.etree.ElementTree as ET
from os import getcwd

sets=[('2007', 'train'), ('2007', 'trainval'), ('2007', 'val')]
#sets=[('2007', 'train'), ('2007', 'val'), ('2007', 'test')]

classes = ["person","rc001","rc002","rc003"]
#classes=["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]

class_num=len(classes)
cls_count=[0]*class_num

person_ids=[]
rc001_ids=[]
rc002_ids=[]
rc003_ids=[]

def convert_annotation(year, image_id, list_file):
    in_file = open('VOCdevkit/VOC%s/Annotations/%s.xml'%(year, image_id))
    tree=ET.parse(in_file)
    root = tree.getroot()
    filen=root.find('filename').text
    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult)==1:
            continue
        cls_id = classes.index(cls)
        cls_count[cls_id]+=1
        if(cls_id==0):
            person_ids.append(image_id)
        if(cls_id==1):
            rc001_ids.append(image_id)
        if(cls_id==2):
            rc002_ids.append(image_id)
        if(cls_id==3):
            rc003_ids.append(image_id)

        xmlbox = obj.find('bndbox')
        b = (int(xmlbox.find('xmin').text), int(xmlbox.find('ymin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymax').text))
        list_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))

wd = getcwd()



for year, image_set in sets:
    image_ids = open('VOCdevkit/VOC%s/ImageSets/Main/%s.txt'%(year, image_set)).read().strip().split()
    list_file = open('%s_%s.txt'%(year, image_set), 'w')
    for image_id in image_ids:
        list_file.write('%s/VOCdevkit/VOC%s/JPEGImages/%s.jpg'%(wd, year, image_id))
        convert_annotation(year, image_id, list_file)
        list_file.write('\n')
    list_file.close()


pfile=open('person_files.txt','w')
for img_id in person_ids:
    pfile.write('%s.jpg'%(img_id))
    pfile.write('\n')
pfile.close()

rc1file=open('rc001_files.txt','w')
for img_id in rc001_ids:
    rc1file.write('%s.jpg'%(img_id))
    rc1file.write('\n')
rc1file.close()

rc2file=open('rc002_files.txt','w')
for img_id in rc002_ids:
    rc2file.write('%s.jpg'%(img_id))
    rc2file.write('\n')
rc2file.close()

rc3file=open('rc003_files.txt','w')
for img_id in rc003_ids:
    rc3file.write('%s.jpg'%(img_id))
    rc3file.write('\n')
rc3file.close()

for i in range(len(classes)):
    print(classes[i],"  count=  ",cls_count[i])