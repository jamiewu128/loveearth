import tkinter as tk
from yolo import YOLO
from PIL import Image,ImageTk
import cv2

def detect_video_frame(vid,yolo):
    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    image = yolo.detect_image(image)
    return image

def diaplay_frame(vid):
    return_value, frame = vid.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    image = Image.fromarray(frame)
    return image

def update_ui():
    img_frame = detect_video_frame(vid,yolo)
    img_show = ImageTk.PhotoImage(image=img_frame)
    w.configure(image=img_show)
    w.image = img_show
    root.after(100,update_ui)

if __name__ == '__main__':

    yolo = YOLO()
    vid = cv2.VideoCapture(0)
    if not vid.isOpened():
        raise IOError("Couldn't open webcam or video")
        yolo.close_session()
        exit()

    root = tk.Tk()
    hi = Image.open('images/hi_i_exist.jpg', 'r')
    imgtk = ImageTk.PhotoImage(image=hi)
    w = tk.Label(root,image=imgtk)
    w.pack()

    root.after(1000,update_ui)
    root.mainloop()